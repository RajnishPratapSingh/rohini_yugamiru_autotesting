﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using System.Runtime.InteropServices;
//using System.Windows.Forms;

namespace CalculatorAutomationWinappDriver
{
    public static class LaunchYugamiru
    {
        public static IntPtr appHandle;
        public static void LaunchNow()
        {
            Process p = new Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardInput = true;
            p.Start();
            p.StandardInput.WriteLine(@"cd C:\Users\suhan\Desktop\Temp\Debug\");
            try
            {
                p.StandardInput.WriteLine("start Yugamiru.exe");
            }
            catch (Exception ex)
            {

            }
            appHandle = p.MainWindowHandle;
        }
    }

    [TestClass]
    public class UnitTest1
    {

        //Appium Driver URL it works like a windows Service on your PC  
        private const string appiumDriverURI = "http://127.0.0.1:4723";

        private const string calApp = @"C:\Users\suhan\Desktop\Temp\Debug\Yugamiru.exe";
        //private const string calApp = @"Yugamiru.exe";

        //WindowsForms10.Window.8.app.0.6255dd_r9_ad1" style=0x56010000 ex=0x50000
        protected static WindowsDriver<WindowsElement> calSession;


        [TestMethod]
        public void LicenseReleaseActivate()
        {
            //LaunchYugamiru.LaunchNow();
            Thread.Sleep(6000);
            if (calSession == null)
            {
                //Process.Start(@"C:\Users\Dell\Desktop\Temp\Release\yugamiru.exe");
                //LaunchYugamiru.LaunchNow();

                DesiredCapabilities appCapabilities = new DesiredCapabilities();
                appCapabilities.SetCapability("app", calApp);
                appCapabilities.SetCapability("deviceName", "WindowsPC");
                calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);
                //Thread.Sleep(6000);

                //var currentWindow1 = calSession.CurrentWindowHandle;
                //var availableWindows1 = new List<string>(calSession.WindowHandles);
                //foreach (string w in availableWindows1)
                //{
                //    if (w != currentWindow1)
                //    {
                //        calSession.SwitchTo().Window(w);
                //    }
                //}
                bool isLicensePage = false;
                bool isAppMainPage = false;
                try
                {
                    calSession.FindElement(By.Name("OK"));
                    isLicensePage = true;
                }
                catch
                {
                    isLicensePage = false;
                }

                #region Release_If_Active
                if (!isLicensePage)
                {
                    //Need to release the existing license
                    calSession.FindElement(By.Name("TOOLS")).Click();
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Down);
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Down);
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Right); //Thread.Sleep(3000);
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Down); //Thread.Sleep(3000);
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Enter);
                    #region commentedOnly1
                    //calSession.FindElement(By.Name("RELEASE LICENSE")).Click();
                    //ClickOnPointTool.ClickOnPoint(LaunchYugamiru.appHandle,System.Windows.Forms.Cursor.Position);
                    //calSession.Mouse.Click(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y
                    //calSession.FindElementByAccessibilityId("toolStripMenuItem_Release_License").Click();
                    //calSession.FindElement(By.Name("TOOLS")).Click(); Thread.Sleep(4000);
                    //calSession.FindElement(By.Name("LICENSE")).Click();
                    //calSession.FindElement(By.Name("RELEASE LICENSE")).Click();
                    //calSession.FindElementByAccessibilityId("bETATESTINGToolStripMenuItem").Click();
                    //calSession.FindElementByAccessibilityId("toolStripMenuItem_License").Click();
                    //calSession.FindElementByAccessibilityId("toolStripMenuItem_Release_License").Click();
                    #endregion
                    var currentWindow1 = calSession.CurrentWindowHandle;
                    var availableWindows1 = new List<string>(calSession.WindowHandles);
                    foreach (string w in availableWindows1)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }
                    calSession.FindElement(By.Name("Release")).Click();
                    var availableWindows2 = new List<string>(calSession.WindowHandles);
                    foreach (string w in availableWindows2)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }
                    calSession.FindElement(By.Name("OK")).Click();
                    Thread.Sleep(5000);
                    //wait for a moment then relaunch the app
                    Thread.Sleep(TimeSpan.FromSeconds(6));
                    LaunchYugamiru.LaunchNow();
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);
                    //return;
                }
                #endregion
                //No License found. Proceed for activation.



                #region Case_Click_Blank
                {
                    Thread.Sleep(3000);
                    calSession.FindElement(By.Name("OK")).Click();
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    string license = " ";
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(license);
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(Keys.Tab);
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(Keys.Tab);
                    calSession.FindElement(By.Name("Activate")).Click();
                    Thread.Sleep(1000);
                    var currentWindow1 = calSession.CurrentWindowHandle;
                    var availableWindows1 = new List<string>(calSession.WindowHandles);
                    foreach (string w in availableWindows1)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }

                    calSession.FindElement(By.Name("OK")).Click();
                    Thread.Sleep(1000);
                    var currentWindow2 = calSession.CurrentWindowHandle;
                    var availableWindows2 = new List<string>(calSession.WindowHandles);
                    foreach (string w in availableWindows2)
                    {
                        if (w != currentWindow2)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }
                    //Now we shud be at Activator screen not on App Home screen if so test shud pass                    
                    try
                    {
                        //if able to access below then it means we are still on activator screen which is expected
                        calSession.FindElementByAccessibilityId("txtLicKey").SendKeys("xyz");
                    }
                    catch
                    {
                        Assert.Fail("Failed at Blank Key Activation Scenario");
                    }

                }
                #endregion

                #region Case_Junk_Key
                {
                    Thread.Sleep(5000);
                    //We should be on Activator Screen only
                    string license = "8F8G3M1PGZH7TEUBUHI0T0P005GF13S"; //junk key (non existing key)
                    calSession.FindElementByAccessibilityId("txtLicKey").Clear();
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(license);
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(Keys.Tab);
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(Keys.Tab);
                    calSession.FindElement(By.Name("Activate")).Click();
                    var currentWindow1 = calSession.CurrentWindowHandle;
                    var availableWindows1 = new List<string>(calSession.WindowHandles);
                    foreach (string w in availableWindows1)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }
                    calSession.FindElement(By.Name("OK")).Click();
                    //Now we shud be at Activator screen not on App Home screen if so test shud pass                    
                    try
                    {
                        //if able to access below then it means we are still on activator screen which is expected
                        calSession.FindElementByAccessibilityId("txtLicKey").SendKeys("xyz");
                    }
                    catch
                    {
                        Assert.Fail("Failed at Junk Key Activation Scenario");
                    }

                }
                #endregion

                #region Case_Expired_Key
                {

                }
                #endregion

                #region Case_another_Trial
                {

                }
                #endregion

                #region Valid_Trial_Key
                {
                    Thread.Sleep(5000);
                    //calSession.FindElement(By.Name("OK")).Click();
                    Thread.Sleep(TimeSpan.FromSeconds(1));
                    string license = "BUHI0T0P005GF13S8F8G3M1PGZH7TEU";
                    calSession.FindElementByAccessibilityId("txtLicKey").Clear();
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(license);
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(Keys.Tab);
                    calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(Keys.Tab);
                    calSession.FindElement(By.Name("Activate")).Click();
                    var currentWindow1 = calSession.CurrentWindowHandle;
                    var availableWindows1 = new List<string>(calSession.WindowHandles);
                    foreach (string w in availableWindows1)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }
                    calSession.FindElement(By.Name("OK")).Click();
                }
                #endregion
                //calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();

                #region hintComments
                //calSession.FindElementByAccessibilityId("IDC_SearchBtn").Click();


                //calSession.FindElementByAccessibilityId("IDC_ID").SetImmediateValue("SUMITID123");
                //calSession.FindElementByAccessibilityId("IDC_Name").Click();
                //calSession.FindElementByAccessibilityId("IDC_Name").SetImmediateValue("SRIVASTAVA");
                //calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
                //calSession.FindElement(By.Name("Start")).Click();IDC_NextBtn
                //calSession.FindElement(By.Name("One")).Click();
                //calSession.FindElement(By.Name("Two")).Click();
                //calSession.FindElement(By.Name("Three")).Click();
                //calSession.FindElement(By.Name("Multiply by")).Click();
                ////find by automation id  
                //calSession.FindElementByAccessibilityId("num9Button").Click();
                //calSession.FindElementByAccessibilityId("equalButton").Click();
                ////getting value from textbox  
                //string ExpectedValue = calSession.FindElementByAccessibilityId("CalculatorResults").Text;
                //string ExpectedValue1 = ExpectedValue.Replace("Display is ", "").Replace(",", "");

                ////Testcases  
                //Assert.AreEqual(82107, Convert.ToInt64(ExpectedValue1));
                #endregion
            }
        }


        [TestMethod]
        public void TestMethod2()
        {
            //if (calSession == null)
            {
                #region CoveredInMethod1
                //Process.Start(@"C:\Users\Dell\Desktop\Temp\Release\yugamiru.exe");
                //Thread.Sleep(6000);
                //DesiredCapabilities appCapabilities = new DesiredCapabilities();
                //appCapabilities.SetCapability("app", calApp);
                //appCapabilities.SetCapability("deviceName", "WindowsPC");
                ////Create a session to intract with Calculator windows application  
                //calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);


                //Automate Button and Get answer from Calculator  

                //find by Name  



                //calSession.SwitchTo().Window("IMAGE_FILE_SELECT");
                #endregion
                //First Image Selection
                calSession.FindElementByAccessibilityId("IDC_SearchBtn").Click();
                var currentWindow1 = calSession.CurrentWindowHandle;
                var availableWindows1 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindows1)
                {
                    if (w != currentWindow1)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("OK")).Click();
                calSession.SwitchTo().Window(currentWindow1);
                calSession.FindElementByAccessibilityId("IDC_ShootBtn").Click();
                calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
                //First Image Selection END

                //Second Image Selection
                calSession.FindElementByAccessibilityId("IDC_SearchBtn").Click();
                var currentWindow2 = calSession.CurrentWindowHandle;
                var availableWindows2 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindows2)
                {
                    if (w != currentWindow2)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("OK")).Click();
                calSession.SwitchTo().Window(currentWindow2);
                calSession.FindElementByAccessibilityId("IDC_ShootBtn").Click();
                calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
                //Second Image Selection END

                //Third Image Selection
                calSession.FindElementByAccessibilityId("IDC_SearchBtn").Click();
                var currentWindow3 = calSession.CurrentWindowHandle;
                var availableWindows3 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindows3)
                {
                    if (w != currentWindow3)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("OK")).Click();
                calSession.SwitchTo().Window(currentWindow3);
                calSession.FindElementByAccessibilityId("IDC_ShootBtn").Click();
                calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
                //Third Image Selection END
                calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
                //Click 'OK' on messagebox 1  START
                var currentWindowM1 = calSession.CurrentWindowHandle;
                var availableWindowsM1 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM1)
                {
                    if (w != currentWindowM1)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("Yes")).Click();
                calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
                //Click 'OK' on messagebox 1  END



                //Click 'OK' on messagebox 2 START
                var currentWindowM2 = calSession.CurrentWindowHandle;
                var availableWindowsM2 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM2)
                {
                    if (w != currentWindowM2)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("Yes")).Click();
                calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
                //Click 'OK' on messagebox 2 END

                //Click 'OK' on messagebox 3 START
                var currentWindowM3 = calSession.CurrentWindowHandle;
                var availableWindowsM3 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM3)
                {
                    if (w != currentWindowM3)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("Yes")).Click();
                calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
                //Click 'OK' on messagebox 3 END


                //Click 'OK' on messagebox 4 START
                var currentWindowM4 = calSession.CurrentWindowHandle;
                var availableWindowsM4 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM4)
                {
                    if (w != currentWindowM4)
                    {
                        calSession.SwitchTo().Window(w);
                    }
                }
                calSession.FindElement(By.Name("Yes")).Click();
                //calSession.FindElementByAccessibilityId("IDC_OkBtn").Click();
                //Click 'OK' on messagebox 4 END

                var currentWindowM45 = calSession.CurrentWindowHandle;
                var availableWindowsM45 = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsM45)
                {
                    if (w != currentWindowM45)
                    {
                        calSession.SwitchTo().Window(w);
                        //break;
                    }
                }
                //calSession.SwitchTo().Window("ResultView");
                //Thread.Sleep(100);
                calSession.FindElementByAccessibilityId("IDC_BTN_DATASAVE").Click();
                //Saved Messagebox START
                var currentWindowMSaved = calSession.CurrentWindowHandle;
                var availableWindowsMSaved = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsMSaved)
                {
                    if (w != currentWindowMSaved)
                    {
                        calSession.SwitchTo().Window(w);
                        //break;
                    }
                }
                Thread.Sleep(5000);
                calSession.FindElement(By.Name("OK")).Click();
                calSession.FindElementByAccessibilityId("IDC_ScoresheetBtn").Click();
                //Saved dialog 'OK' click  END

                //Trial Purchase dialog 'No' click  START
                Thread.Sleep(6000);
                var currentWindowMPurchase = calSession.CurrentWindowHandle;
                var availableWindowsMPurchase = new List<string>(calSession.WindowHandles);

                foreach (string w in availableWindowsMPurchase)
                {
                    if (w != currentWindowMPurchase)
                    {
                        calSession.SwitchTo().Window(w);
                        //break;
                    }
                }
                //calSession.Close();
                //calSession.FindElement(By.Name("Close")).Click();
                //calSession.FindElement(By.Name("No")).Click();
                //Trial Purchase dialog 'No' click  END

                #region hintComments
                //IDC_NextBtn   IDC_OkBtn  IDC_BTN_DATASAVE
                //calSession.FindElementByAccessibilityId("IDC_Name").Click();
                //calSession.FindElementByAccessibilityId("IDC_Name").SendKeys("SRIVASTAVA");
                //calSession.FindElementByAccessibilityId("IDC_ID").Click();
                //calSession.FindElementByAccessibilityId("IDC_ID").SendKeys("SUMITID123");
                //calSession.FindElementByAccessibilityId("IDC_COMBO_GENDER").Click();
                //calSession.FindElementByAccessibilityId("IDC_COMBO_GENDER").SendKeys(Keys.Down);

                //Thread.Sleep(6000);
                //calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();



                //calSession.FindElementByAccessibilityId("IDC_ID").SetImmediateValue("SUMITID123");
                //calSession.FindElementByAccessibilityId("IDC_Name").Click();
                //calSession.FindElementByAccessibilityId("IDC_Name").SetImmediateValue("SRIVASTAVA");
                //calSession.FindElementByAccessibilityId("IDC_NextBtn").Click();
                //calSession.FindElement(By.Name("Start")).Click();IDC_NextBtn
                //calSession.FindElement(By.Name("One")).Click();
                //calSession.FindElement(By.Name("Two")).Click();
                //calSession.FindElement(By.Name("Three")).Click();
                //calSession.FindElement(By.Name("Multiply by")).Click();
                ////find by automation id  
                //calSession.FindElementByAccessibilityId("num9Button").Click();
                //calSession.FindElementByAccessibilityId("equalButton").Click();
                ////getting value from textbox  
                //string ExpectedValue = calSession.FindElementByAccessibilityId("CalculatorResults").Text;
                //string ExpectedValue1 = ExpectedValue.Replace("Display is ", "").Replace(",", "");

                ////Testcases  
                //Assert.AreEqual(82107, Convert.ToInt64(ExpectedValue1));

                #endregion
            }
        }

        [TestMethod]
        public void Licenseview()
        {
            //LaunchYugamiru.LaunchNow();
            Thread.Sleep(6000);
            if (calSession == null)
            {
                //Process.Start(@"C:\Users\Dell\Desktop\Temp\Release\yugamiru.exe");
                //LaunchYugamiru.LaunchNow();

                DesiredCapabilities appCapabilities = new DesiredCapabilities();
                appCapabilities.SetCapability("app", calApp);
                appCapabilities.SetCapability("deviceName", "WindowsPC");
                calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);

                bool isLicensePage = false;
                bool isAppMainPage = false;
                try
                {
                    calSession.FindElement(By.Name("OK"));
                    isLicensePage = true;
                }
                catch
                {
                    isLicensePage = false;
                }

                #region Release_If_Active
                if (!isLicensePage)
                {
                    //Need to release the existing license
                    calSession.FindElement(By.Name("TOOLS")).Click();
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Down);
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Down);
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Right); //Thread.Sleep(3000);
                                                                                   // calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Down); //Thread.Sleep(3000);
                    calSession.FindElement(By.Name("TOOLS")).SendKeys(Keys.Enter);
                    #region commentedOnly1
                    //calSession.FindElement(By.Name("RELEASE LICENSE")).Click();
                    //ClickOnPointTool.ClickOnPoint(LaunchYugamiru.appHandle,System.Windows.Forms.Cursor.Position);
                    //calSession.Mouse.Click(System.Windows.Forms.Cursor.Position.X, System.Windows.Forms.Cursor.Position.Y
                    //calSession.FindElementByAccessibilityId("toolStripMenuItem_Release_License").Click();
                    //calSession.FindElement(By.Name("TOOLS")).Click(); Thread.Sleep(4000);
                    //calSession.FindElement(By.Name("LICENSE")).Click();
                    //calSession.FindElement(By.Name("RELEASE LICENSE")).Click();
                    //calSession.FindElementByAccessibilityId("bETATESTINGToolStripMenuItem").Click();
                    //calSession.FindElementByAccessibilityId("toolStripMenuItem_License").Click();
                    //calSession.FindElementByAccessibilityId("toolStripMenuItem_Release_License").Click();
                    #endregion
                    var currentWindow1 = calSession.CurrentWindowHandle;
                    var availableWindows1 = new List<string>(calSession.WindowHandles);
                    foreach (string w in availableWindows1)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }

                    Thread.Sleep(5000);
                    calSession.FindElement(By.Name("Refresh")).Click();
                    Thread.Sleep(6000);
                    var availableWindows2 = new List<string>(calSession.WindowHandles);
                    foreach (string w in availableWindows2)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }

                    calSession.FindElement(By.Name("Close")).Click();
                    Thread.Sleep(5000);
                    //wait for a moment then relaunch the app
                    Thread.Sleep(TimeSpan.FromSeconds(6));
                    LaunchYugamiru.LaunchNow();
                    Thread.Sleep(TimeSpan.FromSeconds(5));
                    calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);
                    //return;
                }
                #endregion

            }
        }

        [TestMethod]
        public void TestMethod3()
        {

            {
                Thread.Sleep(6000);
                if (calSession == null)
                {
                   

                    DesiredCapabilities appCapabilities = new DesiredCapabilities();
                    appCapabilities.SetCapability("app", calApp);
                    appCapabilities.SetCapability("deviceName", "WindowsPC");
                    calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);
                  
                    calSession.FindElementByAccessibilityId("IDC_SETTING_BTN").Click();
                   

                    var currentWindow1 = calSession.CurrentWindowHandle;
                    var availableWindows1 = new List<string>(calSession.WindowHandles);

                    foreach (string w in availableWindows1)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }
                    Thread.Sleep(5000);
                    calSession.FindElementByAccessibilityId("comboBox1").Click();
                    calSession.FindElementByAccessibilityId("comboBox1").SendKeys(Keys.Down);
                    calSession.FindElementByAccessibilityId("IDC_RADIO_ARROW_ON").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_CENTERLINE_OFF").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_LABEL_OFF").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_CENTROID_ON").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_SCORE_OFF").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_MUSCLEREPORT_ON").Click();
                    Thread.Sleep(4000);
                    calSession.FindElementByAccessibilityId("IDOK").Click();

                }
            }
        }

        [TestMethod]
        public void TestMethod4()
        {

            {
                Thread.Sleep(6000);
                if (calSession == null)
                {


                    DesiredCapabilities appCapabilities = new DesiredCapabilities();
                    appCapabilities.SetCapability("app", calApp);
                    appCapabilities.SetCapability("deviceName", "WindowsPC");
                    calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);

                    calSession.FindElementByAccessibilityId("IDC_SETTING_BTN").Click();


                    var currentWindow1 = calSession.CurrentWindowHandle;
                    var availableWindows1 = new List<string>(calSession.WindowHandles);

                    foreach (string w in availableWindows1)
                    {
                        if (w != currentWindow1)
                        {
                            calSession.SwitchTo().Window(w);
                        }
                    }
                    Thread.Sleep(5000);
                    calSession.FindElementByAccessibilityId("comboBox1").Click();
                    calSession.FindElementByAccessibilityId("comboBox1").SendKeys(Keys.Down);
                    calSession.FindElementByAccessibilityId("comboBox1").SendKeys(Keys.Down);
                    calSession.FindElementByAccessibilityId("IDC_RADIO_ARROW_ON").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_CENTERLINE_OFF").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_LABEL_OFF").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_SCORE_OFF").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_MUSCLEREPORT_ON").Click();
                    calSession.FindElementByAccessibilityId("IDC_RADIO_CENTROID_ON").Click();
                   
                    Thread.Sleep(4000);
                    calSession.FindElementByAccessibilityId("IDCANCEL").Click();
                    Thread.Sleep(4000);

                    calSession.FindElement(By.Name("Yes")).Click();
                    


                }
            }
        }

        [TestMethod]
        public void TestMethod5()
        {

            {
                Thread.Sleep(6000);
                if (calSession == null)
                {
                  
                    DesiredCapabilities appCapabilities = new DesiredCapabilities();
                    appCapabilities.SetCapability("app", calApp);
                    appCapabilities.SetCapability("deviceName", "WindowsPC");
                    calSession = new WindowsDriver<WindowsElement>(new Uri(appiumDriverURI), appCapabilities);
                   
                    bool isLicensePage = false;
                    bool isAppMainPage = false;
                    try
                    {
                        calSession.FindElement(By.Name("OK"));
                        isLicensePage = true;
                    }
                    catch
                    {
                        isLicensePage = false;
                    }

                    {
                        Thread.Sleep(5000);                      
                        Thread.Sleep(TimeSpan.FromSeconds(1));
                        // string license = "B8GM090K0051713R8K8F321B6ZEBUJT";
                         string license = "BGGR0H0U0051C13P8G8G3R1Q6FGFI6X";
                        calSession.FindElementByAccessibilityId("txtLicKey").Clear();
                        calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(license);
                        Thread.Sleep(TimeSpan.FromSeconds(2));
                        calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(Keys.Tab);
                        calSession.FindElementByAccessibilityId("txtLicKey").SendKeys(Keys.Tab);
                        calSession.FindElement(By.Name("Activate")).Click();
                        var currentWindow1 = calSession.CurrentWindowHandle;
                        var availableWindows1 = new List<string>(calSession.WindowHandles);
                        foreach (string w in availableWindows1)
                        {
                            if (w != currentWindow1)
                            {
                                calSession.SwitchTo().Window(w);
                            }
                        }
                        calSession.FindElement(By.Name("OK")).Click();
                        Thread.Sleep(5000);
                        calSession.FindElement(By.Name("Close")).Click();
                    }

                }
            }
        }
    } 
}
